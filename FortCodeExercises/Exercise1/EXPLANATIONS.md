﻿
1. Renamed all the functions and variables with appropriate naming conventions.
2. Made "max" variable in "GetMaxSpeed" as constant since there is a set value of 70.
3. Removed the need of "machineType" parameter as new implementation determines the "machineType" based on the passed "MachineName" Value
4. Using hard coded strings for colors is not a good practice and it is hard to maintain the code. Extracted the colors string to ColorPalette enum with a defined set of enum values.
5. In the functions I have used switch statements instead of if else statements. If the number of conditions exceeds more than 5 then switch statement is preferred for the best performance of the code. Also, it is easier for other developers to read the code.
6. The properties: "TrimColor", "Color", "Description" and "Name" had implementation logic in their get method. Extracted the method to separate private methods.
7. To build the value for "Description" property, the string variable was used. This is a bad practice as strings in C# are immutable. Best way to build the string of values will be by using StringBuilder class.
8. Renamed "type" to "_machineType" and made it readonly private field. Renamed "Name" property to  "MachineName" and changed its type to  "MachineName" which is an enum. 
9. Created a public constructor to set the value of "MachineName" and "_machineType" so that the values are set during the object instantiation.
10. Removed the need for figuring out the "MachineName" based on the "_machineType" by allowing user to supply the "MachineName" during the Machine object initialization. 