﻿namespace FortCodeExercises.Exercise1
{
    public enum ColorPalette
    {
        Red,
        Blue,
        Green,
        Yellow,
        Brown,
        Black,
        White,
        Gold,
        Silver,
        Beige,
        BabyBlue,
        Crimson
    }
}
