using System;
using System.Text;

namespace FortCodeExercises.Exercise1
{
    public class Machine
    {
        private readonly int _machineType;

        public Machine(MachineName machineName)
        {
            MachineName = machineName;
            _machineType = (int)MachineName;
        }

        public MachineName MachineName { get; }
        public string Description => GetDescription();
        public string Color => GetColorName();

        private string GetDescription()
        {
            var hasMaxSpeed = true;

            switch (_machineType)
            {
                case 1:
                case 2:
                    hasMaxSpeed = true; //I could have removed this assignment as it is redundant but kept it as it is to meet the code logic.
                    break;
                case 3:
                case 4:
                    hasMaxSpeed = false;
                    break;
            }

            var descriptionBuilder = new StringBuilder(" "); //Use StringBuilder instead of String

            descriptionBuilder.Append(Color + " ");
            descriptionBuilder.Append(MachineName + " " + "[");
            descriptionBuilder.Append(GetMaxSpeed(hasMaxSpeed) + "].");
            return descriptionBuilder.ToString();
        }

        private string GetColorName()
        {
            string color;
            switch (_machineType)
            {
                case 0:
                    color = ColorPalette.Red.ToString();
                    break;
                case 1:
                    color = ColorPalette.Blue.ToString();
                    break;
                case 2:
                    color = ColorPalette.Green.ToString();
                    break;
                case 3:
                    color = ColorPalette.Yellow.ToString();
                    break;
                case 4:
                    color = ColorPalette.Brown.ToString();
                    break;
                default:
                    color = ColorPalette.White.ToString();
                    break;
            }
            return color;
        }

        public string TrimColor => GetTrimColor();

        private string GetTrimColor()
        {
            var baseColor = GetBaseColor();

            var trimColor = "";
            switch (_machineType)
            {
                case 1 when IsDark(baseColor):
                    trimColor = ColorPalette.Black.ToString();
                    break;
                case 1 when !IsDark(baseColor):
                    trimColor = ColorPalette.White.ToString();
                    break;
                case 2 when IsDark(baseColor):
                    trimColor = ColorPalette.Gold.ToString();
                    break;
                case 3 when IsDark(baseColor):
                    trimColor = ColorPalette.Silver.ToString();
                    break;
            }
            return trimColor;
        }

        private ColorPalette GetBaseColor()
        {
            switch (_machineType)
            {
                case 0:
                    return ColorPalette.Red;
                case 1:
                    return ColorPalette.Blue;
                case 2:
                    return ColorPalette.Green;
                case 3:
                    return ColorPalette.Yellow;
                case 4:
                    return ColorPalette.Brown;
                default:
                    return ColorPalette.White;
            }
        }

        public bool IsDark(ColorPalette color)
        {
            switch (color)
            {
                case ColorPalette.Red:
                    return true;
                case ColorPalette.Yellow:
                    return false;
                case ColorPalette.Green:
                case ColorPalette.Black:
                    return true;
                case ColorPalette.White:
                case ColorPalette.Beige:
                case ColorPalette.BabyBlue:
                    return false;
                case ColorPalette.Crimson:
                    return true;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public int GetMaxSpeed(bool noMax = false)
        {
            const int max = 70;

            if (!noMax && _machineType == 1)
            {
                return max;
            }

            if (!noMax && _machineType == 2)
            {
                return 60;
            }

            switch (_machineType)
            {
                case 0 when noMax:
                    return 80;
                case 1 when noMax:
                    return 75;
                case 2 when noMax:
                case 4 when noMax:
                    return 90;

                default:
                    return max;
            }
        }
    }
}